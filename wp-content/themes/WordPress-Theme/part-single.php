<!-- Begin Content -->
	<section class="content wow fadeIn special" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns single_main">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<div class="single_thumbnail text-center"><?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?></div>
					<div class="single_content">
						<?php the_title( '<h1 class="text-center">', '</h1>' ); ?>
						<?php the_content(); ?>
					</div>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
<!-- End Content -->